import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hospital_app/configs/ioc.dart';
import 'package:flutter_hospital_app/services/database.service.dart';
import 'package:flutter_hospital_app/store/store.dart';

class Environment {
  static setup() async {
    // Make sure that the binary messenger binding are properly initialiazed
    WidgetsFlutterBinding.ensureInitialized();

    // lock orientation position
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    // transparent status bar
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
    ));

    Ioc.setupIocDependency();
    await Ioc.get<SqliteDatabaseService>().initDatabase();

    return await createStore();
  }
}
