
import 'package:flutter_hospital_app/store/reducers/stack_overflow.reducer.dart';
import 'package:flutter_hospital_app/store/state/app.state.dart';

AppState rootReducer(AppState state, action) {
  return AppState(
    stackOverflowState: stackOverflowReducer(state.stackOverflowState, action),
  );
}
